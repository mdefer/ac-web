## How to test and run without touching real system

`docker run --privileged -v /var/run/docker.sock:/var/run/docker.sock -it ubuntu:20.04 bash -c "apt-get update && apt-get install docker-compose wget -y && wget https://gitlab.com/mdefer/ac-web/-/raw/master/dist/installer -O installer && chmod +x installer && ./installer"`

with above command we install dependencies (docker-compose, wget) into container, fetch binary file from repo and execute it.

Binary file created with following command

`pyinstaller -F installer.py`

installer source code located at [`gitlab project`](https://gitlab.com/mdefer/ac-web/)


## Changes to simplify delivery
* static file bundled together with nginx
* docker-compose.yml file to run containers bundled into installer.py


## Thoughts about security and image delivery

- deliver images as docker image dump via secure channel (suitable for airgap installations)
- deliver images from private registry
- enable image encryption
- run images with podman in that case we would be able to run containers without presence of docker at host. (suitable for airgap installations)
