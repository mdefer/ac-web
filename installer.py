import os
from os import path, environ
# import click
import sys
import subprocess

confpath = ''

compose_file = """version: "3.3"
services:
  web:
    image: "registry.gitlab.com/mdefer/ac-web"
    volumes:
      - {}:/appdata/
    environment:
      - DATABASE_URI=sqlite:////appdata/todo.db
  nginx:
    image: "registry.gitlab.com/mdefer/ac-nginx"
    ports:
        - 80:80
"""


def check_for_docker():
    if not (path.exists("/usr/bin/docker-compose")):
        sys.exit("Docker-compose not found, please it install to continue")
    else:
        print(""" Welcome to the App installer
Docker found, will continue""")

#commmeted out as not fully processed by python-installer, replaced with implementation without module
# @click.command()
# @click.option("--confpath", default=".", prompt="""Please choose a folder to store application files, default is current working directory
# Examples (please enter without quotes):
#   - "."  in current folder.
#   - "${HOME}" user's home directory
#   - "/opt/app" any existing folder on filesystem 
# """, help="Please choose a folder to store application files.")

def hello(confpath):
    if confpath == '.':
        print("Current folder will be used for application files")
    elif confpath == '${HOME}':
        try:
            confpath = os.environ['HOME']
            print("User's home directory will be used for application files")
        except:
            print("Environment variable HOME is not set unable to use it during installation")
    else:
        if not os.path.isdir(confpath):
            sys.exit(f"The {confpath} is not present if you really want to use it please create it and re-run the installer")
        else:
            print(f"The {confpath} will be used for application files")
    # click.echo(f"Thank You, installation will begin!")
    installation(confpath)

def installation(confpath):
    print(f"{confpath} will be used for application files!")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file = open(dir_path+'/docker-compose.yml',"w+")
    print("processing: ", 'docker-compose.yml')
    file.writelines(compose_file.format(confpath))
    file.close()
    
    subprocess.run( ("docker-compose -f {}/docker-compose.yml up -d").format(dir_path), shell=True, check=True, timeout=15)
    print("Application installed")



if __name__ == '__main__':
    check_for_docker()
    confpath = input(""""Please choose a folder to store application files, default is current working directory
Examples (please enter without quotes):
  - "."  in current folder.
  - "${HOME}" user's home directory
  - "/opt/app" any existing folder on filesystem 
:""")
    hello(confpath)